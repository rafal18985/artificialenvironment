using System.IO;
using Configuration;
using LifeSupporters;
using ML;
using Pools;
using UnityEngine;
using Validators.Base;

namespace Saves
{
    public class SaveManager : MonoBehaviour
    {
        [SerializeField] private TrainingConfiguration trainingConfiguration;

        [SerializeField] private CrowdLimiterValidator plantCrowdLimiterValidator;
        [SerializeField] private CrowdLimiterValidator herbivoreCrowdLimiterValidator;
        [SerializeField] private CrowdLimiterValidator carnivoreCrowdLimiterValidator;
        [SerializeField] private CrowdLimiterValidator allActorsCrowdLimiterValidator;

        [SerializeField] private PlantLifeSupporter plantLifeSupporter;
        [SerializeField] private CreatureLifeSupporter herbivoreLifeSupporter;
        [SerializeField] private CreatureLifeSupporter carnivoreLifeSupporter;

        [SerializeField] private PlantPool plantPool;
        [SerializeField] private CreaturePool herbivorePool;
        [SerializeField] private CreaturePool carnivorePool;

        [SerializeField] private string configurationFileName = "configuration.json";
        
        #if UNITY_EDITOR
        private string SavePath => $"{Application.dataPath}/{configurationFileName}";
        #else
        private string SavePath => $"{Application.dataPath}/../{configurationFileName}";
        #endif

        private void Start()
        {
            Load();

            plantPool.ObjectInstantiated +=
                (sender, plant) =>
                {
                    plant.energy.energyConfiguration = trainingConfiguration.plantConfiguration.energyConfiguration;
                    plant.reproductionConfiguration =
                        trainingConfiguration.plantConfiguration.reproductionConfiguration;
                };

            herbivorePool.ObjectInstantiated += (sender, herbivore) =>
                {
                    herbivore.energy.energyConfiguration =
                        trainingConfiguration.herbivoreConfiguration.energyConfiguration;
                    herbivore.reproductionConfiguration =
                        trainingConfiguration.herbivoreConfiguration.reproductionConfiguration;
                    herbivore.attributesConfiguration =
                        trainingConfiguration.herbivoreConfiguration.creatureAttributesConfiguration;

                    var agent = herbivore.GetComponent<CreatureAgent>();
                    if (agent != null)
                    {
                        agent.rewardConfiguration = trainingConfiguration.herbivoreConfiguration.rewardConfiguration;
                    }
                };
            
            
            carnivorePool.ObjectInstantiated += (sender, carnivore) =>
            {
                carnivore.energy.energyConfiguration =
                    trainingConfiguration.carnivoreConfiguration.energyConfiguration;
                carnivore.reproductionConfiguration =
                    trainingConfiguration.carnivoreConfiguration.reproductionConfiguration;
                carnivore.attributesConfiguration =
                    trainingConfiguration.carnivoreConfiguration.creatureAttributesConfiguration;

                var agent = carnivore.GetComponent<CreatureAgent>();
                if (agent != null)
                {
                    agent.rewardConfiguration = trainingConfiguration.carnivoreConfiguration.rewardConfiguration;
                }
            };

            plantCrowdLimiterValidator.populationConfiguration =
                trainingConfiguration.plantConfiguration.populationConfiguration;
            herbivoreCrowdLimiterValidator.populationConfiguration =
                trainingConfiguration.herbivoreConfiguration.populationConfiguration;
            carnivoreCrowdLimiterValidator.populationConfiguration =
                trainingConfiguration.carnivoreConfiguration.populationConfiguration;
            allActorsCrowdLimiterValidator.populationConfiguration =
                trainingConfiguration.allActorPopulationConfiguration;

            plantLifeSupporter.populationConfiguration =
                trainingConfiguration.plantConfiguration.populationConfiguration;
            herbivoreLifeSupporter.populationConfiguration =
                trainingConfiguration.herbivoreConfiguration.populationConfiguration;
            carnivoreLifeSupporter.populationConfiguration =
                trainingConfiguration.carnivoreConfiguration.populationConfiguration;

        }
        
        private void OnApplicationQuit()
        {
            Save();
        }

        private void Load()
        {
            if(trainingConfiguration == null)
                trainingConfiguration = ScriptableObject.CreateInstance<TrainingConfiguration>();


            if (File.Exists(SavePath))
            {
                var jsonString = File.ReadAllText(SavePath);
                JsonUtility.FromJsonOverwrite(jsonString, trainingConfiguration);
            }
        }
        
        private void Save()
        {
            var jsonString = JsonUtility.ToJson(trainingConfiguration, true);
            File.WriteAllText(SavePath, jsonString);
        }
    }
}