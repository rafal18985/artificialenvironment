using System;
using Actors;
using SO_Architecture.Collections;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

public class FollowRandomCreatureCamera : MonoBehaviour
{
    [SerializeField] private new Camera camera;
    [SerializeField] private ActorCollection herbivoresCollection;
    [SerializeField] private ActorCollection carnivoresCollection;
    [SerializeField] private Vector3 offset = new Vector3(0, 20, 0);
    [SerializeField] private float zoomSpeed = 2;
    private float minOffset = 20;
    private float maxOffset = 200;
    
    
    
    private Actor target;
    
    private void Update()
    {
        if (target == null || !target.gameObject.activeInHierarchy)
        {
            ChooseNewTarget();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (!EventSystemUtils.IsUserTyping())
                ChooseRandomFromCollection(herbivoresCollection);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            if (!EventSystemUtils.IsUserTyping())
                ChooseRandomFromCollection(carnivoresCollection);
        }
        
        var mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        if (mouseWheel != 0)
        {
            float extraZoomMultiplier = 1;
            if (Input.GetKey(KeyCode.LeftControl))
                extraZoomMultiplier = 5;
            
            if (!EventSystemUtils.IsUserTyping())
                offset.y = Mathf.Clamp(offset.y - mouseWheel * zoomSpeed * extraZoomMultiplier, minOffset, maxOffset);
        }

        if(target != null)
            FollowTarget();
    }

    private void FollowTarget()
    {
        camera.transform.position = target.transform.position + offset;
        camera.transform.LookAt(target.transform);
    }

    private void ChooseNewTarget()
    {
        int actorCount = herbivoresCollection.Count + carnivoresCollection.Count;
        if (actorCount == 0)
        {
            target = null;
            return;
        }
        
        int randomIndex = Random.Range(0, actorCount);

        if (randomIndex < herbivoresCollection.Count)
        {
            target = herbivoresCollection[randomIndex];
        }
        else
        {
            target = carnivoresCollection[randomIndex - herbivoresCollection.Count];
        }
    }

    private void ChooseRandomFromCollection(ActorCollection actorCollection)
    {
        if(actorCollection.Count == 0)
            return;
        target = actorCollection[Random.Range(0, actorCollection.Count)];
    }
}