﻿using Actors;
using Pools;
using Spawners;
using UnityEngine;

namespace Reproducers
{
    [CreateAssetMenu(fileName = "CreatureReproducer", menuName = "Reproducers/Creature", order = 0)]
    public class CreatureReproducer : Reproducer<Creature, CreaturePool, CreatureSpawner>
    {
    }
}
