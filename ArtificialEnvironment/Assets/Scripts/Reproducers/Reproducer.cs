﻿using System.Collections.Generic;
using System.Linq;
using Actors;
using Generators;
using Generators.Base;
using Pools;
using Spawners;
using UnityEngine;
using Validators.Base;

namespace Reproducers
{
    public abstract class Reproducer<T, TPool, TSpawner> : ScriptableObject where T : Actor
        where TPool : Pool<T>
        where TSpawner : Spawner<T, TPool>
    {
        public TSpawner spawner;

        public List<ActorValidator> canReproduceValidators;

        public InRadiusInBoundsGenerator childPositionGenerator;


        public bool CanReproduce(T actor)
        {
            return canReproduceValidators.All(canReproduceValidator => canReproduceValidator.IsValid(actor));
        }

        public bool TryToReproduce(T actor)
        {
            if (!CanReproduce(actor))
            {
                return false;
            }

            var childPosition = childPositionGenerator.Generate(actor.transform.position);

            if (!spawner.CanSpawnAtPosition(childPosition))
            {
                actor.energy.Spend(actor.reproductionConfiguration.reproduceCost);
                return false;
            }

            var child = spawner.SpawnAtPosition(childPosition);
            actor.energy.TransferTo(child.energy, actor.reproductionConfiguration.reproduceCost,
                actor.reproductionConfiguration.reproduceEfficiency);

            actor.timeSinceLastReproduce = 0;

            return true;
        }
    }
}
