﻿using Actors;
using Pools;
using Spawners;
using UnityEngine;

namespace Reproducers
{
    [CreateAssetMenu(fileName = "PlantReproducer", menuName = "Reproducers/Plant", order = 0)]
    public class PlantReproducer : Reproducer<Plant, PlantPool, PlantSpawner>
    {
    }
}
