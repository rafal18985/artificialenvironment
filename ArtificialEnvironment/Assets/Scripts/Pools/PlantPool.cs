﻿using Actors;
using UnityEngine;

namespace Pools
{
    [CreateAssetMenu(fileName = "PlantPool", menuName = "Pools/Plant", order = 0)]
    public class PlantPool : Pool<Plant>
    {
    }
}
