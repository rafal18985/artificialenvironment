﻿using Actors;
using UnityEngine;

namespace Pools
{
    [CreateAssetMenu(fileName = "CreaturePool", menuName = "Pools/Creature", order = 0)]
    public class CreaturePool : Pool<Creature>
    {
        
    }
}