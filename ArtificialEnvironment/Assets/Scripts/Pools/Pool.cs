﻿using System;
using System.Collections.Generic;
using Actors;
using UnityEngine;

namespace Pools
{
    public abstract class Pool<T> : ScriptableObject, IPool<T> where T : Actor 
    {
        public T prefab;

        public event EventHandler<T> ObjectInstantiated;
        
        private Transform parent;
        private Queue<T> PoolQueue { get; } = new Queue<T>();

        public T Get()
        {
            if (PoolQueue.Count == 0)
            {
                Populate(1);
            }

            var pooledObject = PoolQueue.Dequeue();
            pooledObject.OnPulledOut();
            return pooledObject;
        }

        public void Populate(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var obj = Instantiate(prefab, parent);
                ObjectInstantiated?.Invoke(this, obj);
                obj.Died += ActorDied;
                Add(obj);
            }
        }

        public void Add(T obj)
        {
            obj.gameObject.SetActive(false);
            PoolQueue.Enqueue(obj);
        }

        public void SetParent(Transform parent)
        {
            this.parent = parent;
        }

        private void ActorDied(object sender, EventArgs args)
        {
            if(sender is T actor)
                Add(actor);
        }
    }
}
