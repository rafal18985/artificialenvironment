using Actors;

namespace Pools
{
    public interface IPool<out T> where T : Actor
    {
        T Get();
        void Populate(int count);
    }
}