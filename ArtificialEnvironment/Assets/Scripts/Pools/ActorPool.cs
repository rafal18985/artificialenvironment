﻿using Actors;
using UnityEngine;

namespace Pools
{
    [CreateAssetMenu(fileName = "ActorPool", menuName = "Pools/Actor", order = 0)]
    public class ActorPool : Pool<Actor>
    {
    }
}
