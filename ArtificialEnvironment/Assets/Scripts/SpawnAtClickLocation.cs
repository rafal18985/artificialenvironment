using Actors;
using Pools;
using Spawners;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpawnAtClickLocation : MonoBehaviour
{
    [SerializeField] private new Camera camera;
    
    public ISpawner<Actor, IPool<Actor>> SelectedSpawner { get; set; }

    
    private void Update()
    {
        if(SelectedSpawner == null)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            if(EventSystem.current.IsPointerOverGameObject())
                return;
            
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform.CompareTag("ground"))
                {
                    SelectedSpawner.SpawnAtPosition(hit.point);
                }
            }
        }
    }
}