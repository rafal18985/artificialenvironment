﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SO_Architecture.Collections;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Renderers
{
    public class ActorCollectionLineRenderer : MonoBehaviour
    {
        public UILineRenderer lineRenderer;
    
        public ActorCollection actorCollection;
        public ActorCollection allActorsCollection;
    
        public int maxSize = 100;
        public float updateInterval = 1f;
    
        private readonly Queue<Vector2> positions = new Queue<Vector2>();

        private void OnEnable()
        {
            StartCoroutine(UpdatePositions());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        IEnumerator UpdatePositions()
        {
            while (true)
            {
                positions.Enqueue(new Vector2(actorCollection.Count, allActorsCollection.Count));
                while (positions.Count > maxSize)
                {
                    positions.Dequeue();
                }

                float maxAllActorCount = positions.Max(position => position.y);
                lineRenderer.Points = (positions
                    .Select((f, i) =>
                    {
                        if (maxAllActorCount == 0)
                            return new Vector2(i / (float) maxSize, 0);
                        
                        return new Vector2(i / (float) maxSize, f.x / maxAllActorCount);
                    }).ToArray());

                yield return new WaitForSeconds(updateInterval);
            }
        }
    }
}
