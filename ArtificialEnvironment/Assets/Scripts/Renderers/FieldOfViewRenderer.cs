﻿using System;
using Actors;
using Statistics;
using UnityEngine;

namespace Renderers
{
    public class FieldOfViewRenderer : MonoBehaviour
    {
        [SerializeField] private MeshFilter meshFilter;
        [SerializeField] private int segments = 20;
        [SerializeField] private Creature creature;
        
        private FieldOfView lastFieldOfView;
        private Mesh mesh;
        
        private void Start()
        {
            mesh = new Mesh();
            meshFilter.mesh = mesh;

            lastFieldOfView = new FieldOfView()
                {
                    angle = creature.attributesConfiguration.eatFieldOfView.angle,
                    range = creature.attributesConfiguration.eatFieldOfView.range
                };
            
            RecalculateVerticesAndTriangles();
        }

        private void Update()
        {
            if (creature.attributesConfiguration.eatFieldOfView.Equals(lastFieldOfView)) 
                return;
            
            RecalculateVerticesAndTriangles();
            lastFieldOfView.angle = creature.attributesConfiguration.eatFieldOfView.angle;
            lastFieldOfView.range = creature.attributesConfiguration.eatFieldOfView.range;
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            if(UnityEditor.PrefabUtility.IsPartOfAnyPrefab(this))
                return;
            
            if(lastFieldOfView == null)
                return;
            
            if(mesh == null)
                return;

            RecalculateVerticesAndTriangles();
        }
        #endif

        private void RecalculateVerticesAndTriangles()
        {
            float startAngle = lastFieldOfView.MinAngle;
            float angleDelta = lastFieldOfView.angle / segments;

            var vertices = new Vector3[segments + 2];
            var triangles = new int[segments * 3];

            vertices[0] = Vector3.zero;
            vertices[1] = DirFromAngle(startAngle) * lastFieldOfView.range;

            for (int i = 0; i < segments; i++)
            {
                vertices[i + 2] = DirFromAngle(startAngle + angleDelta * (i + 1)) * lastFieldOfView.range;

                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;

            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;
        }

        private Vector3 DirFromAngle(float angle)
        {
            return new Vector3(
                Mathf.Sin(Mathf.Deg2Rad * angle),
                0,
                Mathf.Cos(Mathf.Deg2Rad * angle));
        }
    }
}
