using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TimeScaleSlider : MonoBehaviour
    {
        [SerializeField] private Slider timeScaleSlider;

        private void Start()
        {
            timeScaleSlider.onValueChanged.AddListener(timeScale => { Time.timeScale = timeScale; });
        }

        private void OnDisable()
        {
            timeScaleSlider.onValueChanged.RemoveAllListeners();
        }
    }
}