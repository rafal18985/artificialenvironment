using System.Globalization;
using Configuration;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class CreatureAttributesConfigurationUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField speedInput;
        [SerializeField] private TMP_InputField turnSpeedInput;
        [SerializeField] private TMP_InputField eatSpeedInput;
        [SerializeField] private TMP_InputField eatAngleInput;
        [SerializeField] private TMP_InputField eatRangeInput;

        private CreatureAttributesConfiguration creatureAttributesConfiguration;
        
        private void OnDestroy()
        {
            UnregisterListeners();
        }

        public void SetConfiguration(CreatureAttributesConfiguration creatureAttributesConfiguration)
        {
            UnregisterListeners();

            if (creatureAttributesConfiguration == null)
            {
                gameObject.SetActive(false);
                return;
            }
            else
            {
                gameObject.SetActive(true);
            }
            
            this.creatureAttributesConfiguration = creatureAttributesConfiguration;
            
            speedInput.text = creatureAttributesConfiguration.speed.ToString(CultureInfo.InvariantCulture);
            turnSpeedInput.text = creatureAttributesConfiguration.turnSpeed.ToString(CultureInfo.InvariantCulture);
            eatSpeedInput.text = creatureAttributesConfiguration.eatSpeed.ToString(CultureInfo.InvariantCulture);
            eatAngleInput.text = creatureAttributesConfiguration.eatFieldOfView.angle.ToString(CultureInfo.InvariantCulture);
            eatRangeInput.text = creatureAttributesConfiguration.eatFieldOfView.range.ToString(CultureInfo.InvariantCulture);
            RegisterListeners();
        }

        private void RegisterListeners()
        {
            speedInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(speedInput, ref creatureAttributesConfiguration.speed));
            turnSpeedInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(turnSpeedInput, ref creatureAttributesConfiguration.turnSpeed));
            eatSpeedInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(eatSpeedInput, ref creatureAttributesConfiguration.eatSpeed));
            eatAngleInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(eatAngleInput, ref creatureAttributesConfiguration.eatFieldOfView.angle));
            eatRangeInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(eatRangeInput, ref creatureAttributesConfiguration.eatFieldOfView.range));
        }

        private void UnregisterListeners()
        {
            speedInput.onEndEdit.RemoveAllListeners();
            turnSpeedInput.onEndEdit.RemoveAllListeners();
            eatSpeedInput.onEndEdit.RemoveAllListeners();
            eatAngleInput.onEndEdit.RemoveAllListeners();
            eatRangeInput.onEndEdit.RemoveAllListeners();
        }
    }
}