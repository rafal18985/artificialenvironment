﻿using Actors;
using Configuration;
using Pools;
using Spawners;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    public class SpeciesSelectionUI : MonoBehaviour
    {
        [SerializeField] private Button plantSelectionButton;
        [SerializeField] private Button herbivoreSelectionButton;
        [SerializeField] private Button carnivoreSelectionButton;

        [SerializeField] private EnergyConfigurationUI energyConfigurationUi;
        [SerializeField] private ReproductionConfigurationUI reproductionConfigurationUi;
        [SerializeField] private PopulationConfigurationUI populationConfigurationUi;
        [SerializeField] private CreatureAttributesConfigurationUI creatureAttributesConfigurationUi;
        [SerializeField] private RewardConfigurationUI rewardConfigurationUi;
        
        [SerializeField] private CreatureSpawner carnivoreSpawner;
        [SerializeField] private CreatureSpawner herbivoreSpawner;
        [SerializeField] private PlantSpawner plantSpawner;
        [SerializeField] private SpawnAtClickLocation spawnAtClickLocation;
        
        [SerializeField] private TrainingConfiguration trainingConfiguration;
        
        private void Start()
        {
            plantSelectionButton.onClick.AddListener(() =>
            {
                ActivateUIFor(trainingConfiguration.plantConfiguration);
                SelectSpawner(plantSpawner);
            });
            herbivoreSelectionButton.onClick.AddListener(() =>
            {
                ActivateUIFor(trainingConfiguration.herbivoreConfiguration);
                SelectSpawner(herbivoreSpawner);
            });
            carnivoreSelectionButton.onClick.AddListener(() =>
            {
                ActivateUIFor(trainingConfiguration.carnivoreConfiguration);
                SelectSpawner(carnivoreSpawner);
            });
        
            ActivateUIFor(trainingConfiguration.plantConfiguration);
            SelectSpawner(plantSpawner);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (!EventSystemUtils.IsUserTyping())
                {
                    ActivateUIFor(trainingConfiguration.plantConfiguration);
                    SelectSpawner(plantSpawner);
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (!EventSystemUtils.IsUserTyping())
                {
                    ActivateUIFor(trainingConfiguration.herbivoreConfiguration);
                    SelectSpawner(herbivoreSpawner);
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (!EventSystemUtils.IsUserTyping())
                {
                    ActivateUIFor(trainingConfiguration.carnivoreConfiguration);
                    SelectSpawner(carnivoreSpawner);
                }
            }
        }



        private void SelectSpawner(ISpawner<Actor, IPool<Actor>> spawner)
        {
            spawnAtClickLocation.SelectedSpawner = spawner;
            plantSelectionButton.interactable = !ReferenceEquals(spawner, plantSpawner);
            herbivoreSelectionButton.interactable = !ReferenceEquals(spawner, herbivoreSpawner);
            carnivoreSelectionButton.interactable = !ReferenceEquals(spawner, carnivoreSpawner);
        }

        private void ActivateUIFor(TrainingConfiguration.ActorConfiguration actorConfiguration)
        {
            energyConfigurationUi.SetConfiguration(actorConfiguration.energyConfiguration);
            reproductionConfigurationUi.SetConfiguration(actorConfiguration.reproductionConfiguration);
            populationConfigurationUi.SetConfiguration(actorConfiguration.populationConfiguration);
            if (actorConfiguration is TrainingConfiguration.CreatureConfiguration creatureConfiguration)
            {
                creatureAttributesConfigurationUi.SetConfiguration(creatureConfiguration.creatureAttributesConfiguration);
                rewardConfigurationUi.SetConfiguration(creatureConfiguration.rewardConfiguration);
            }
            else
            {
                creatureAttributesConfigurationUi.SetConfiguration(null);
                rewardConfigurationUi.SetConfiguration(null);
            }
        }

        private void OnDestroy()
        {
            plantSelectionButton.onClick.RemoveAllListeners();
            herbivoreSelectionButton.onClick.RemoveAllListeners();
            carnivoreSelectionButton.onClick.RemoveAllListeners();
        }
    }
}
