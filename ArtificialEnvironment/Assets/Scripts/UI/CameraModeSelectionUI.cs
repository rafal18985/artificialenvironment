﻿using MLAgents;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class CameraModeSelectionUI : MonoBehaviour
{
    [SerializeField] private Button followModeSelectionButton;
    [SerializeField] private Button flyModeSelectionButton;

    [SerializeField] private FollowRandomCreatureCamera followCamera;
    [SerializeField] private FlyCamera flyCamera;
    
    private void Start()
    {
        followModeSelectionButton.onClick.AddListener(() =>
        {
            SelectCamera(CameraMode.Follow);
        });
        flyModeSelectionButton.onClick.AddListener(call: () =>
        {
            SelectCamera(CameraMode.Fly);
        });
        
        SelectCamera(CameraMode.Follow);
    }
    
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (!EventSystemUtils.IsUserTyping())
            {
                SelectCamera(CameraMode.Follow);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (!EventSystemUtils.IsUserTyping())
            {
                SelectCamera(CameraMode.Fly);
            }
        }
    }
    

    private void SelectCamera(CameraMode cameraMode)
    {
        if (cameraMode == CameraMode.Follow)
        {
            flyCamera.enabled = false;
            followCamera.enabled = true;
            followModeSelectionButton.interactable = false;
            flyModeSelectionButton.interactable = true;
        }
        else
        {
            followCamera.enabled = false;
            flyCamera.enabled = true;
            followModeSelectionButton.interactable = true;
            flyModeSelectionButton.interactable = false;
        }
    }

    private enum CameraMode
    {
        Follow,
        Fly
    }
}
