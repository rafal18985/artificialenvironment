using System.Globalization;
using Configuration;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class RewardConfigurationUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField dieInput;
        [SerializeField] private TMP_InputField foodEatenInput;
        [SerializeField] private TMP_InputField reproduceInput;
        [SerializeField] private TMP_InputField existentialInput;

        private RewardConfiguration rewardConfiguration;
        
        private void OnDestroy()
        {
            UnregisterListeners();
        }

        public void SetConfiguration(RewardConfiguration rewardConfiguration)
        {
            UnregisterListeners();

            if (rewardConfiguration == null)
            {
                gameObject.SetActive(false);
                return;
            }
            else
            {
                gameObject.SetActive(true);
            }
            
            this.rewardConfiguration = rewardConfiguration;
            
            dieInput.text = rewardConfiguration.dieReward.ToString(CultureInfo.InvariantCulture);
            foodEatenInput.text = rewardConfiguration.foodEatenReward.ToString(CultureInfo.InvariantCulture);
            reproduceInput.text = rewardConfiguration.reproduceReward.ToString(CultureInfo.InvariantCulture);
            existentialInput.text = rewardConfiguration.existentialReward.ToString(CultureInfo.InvariantCulture);
            RegisterListeners();
        }

        private void RegisterListeners()
        {
            dieInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(dieInput, ref rewardConfiguration.dieReward));
            foodEatenInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(foodEatenInput, ref rewardConfiguration.foodEatenReward));
            reproduceInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(reproduceInput, ref rewardConfiguration.reproduceReward));
            existentialInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(existentialInput, ref rewardConfiguration.existentialReward));
        }

        private void UnregisterListeners()
        {
            dieInput.onEndEdit.RemoveAllListeners();
            foodEatenInput.onEndEdit.RemoveAllListeners();
            reproduceInput.onEndEdit.RemoveAllListeners();
            existentialInput.onEndEdit.RemoveAllListeners();
        }
    }
}