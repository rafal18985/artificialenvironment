using System.Globalization;
using Configuration;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class ReproductionConfigurationUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField reproductionCostInput;
        [SerializeField] private TMP_InputField reproductionEfficiencyInput;
        [SerializeField] private TMP_InputField reproductionReserveInput;
        [SerializeField] private TMP_InputField reproductionCooldownInput;

        private ReproductionConfiguration reproductionConfiguration;
        
        private void OnDestroy()
        {
            UnregisterListeners();
        }

        public void SetConfiguration(ReproductionConfiguration reproductionConfiguration)
        {
            UnregisterListeners();
            this.reproductionConfiguration = reproductionConfiguration;
            
            reproductionCostInput.text = reproductionConfiguration.reproduceCost.ToString(CultureInfo.InvariantCulture);
            reproductionEfficiencyInput.text = reproductionConfiguration.reproduceEfficiency.ToString(CultureInfo.InvariantCulture);
            reproductionReserveInput.text = reproductionConfiguration.energyReserve.ToString(CultureInfo.InvariantCulture);
            reproductionCooldownInput.text = reproductionConfiguration.reproduceCooldown.ToString(CultureInfo.InvariantCulture);
            RegisterListeners();
        }

        private void RegisterListeners()
        {
            reproductionCostInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(reproductionCostInput, ref reproductionConfiguration.reproduceCost));
            reproductionEfficiencyInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(reproductionEfficiencyInput, ref reproductionConfiguration.reproduceEfficiency));
            reproductionReserveInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(reproductionReserveInput, ref reproductionConfiguration.energyReserve));
            reproductionCooldownInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(reproductionCooldownInput, ref reproductionConfiguration.reproduceCooldown));
        }

        private void UnregisterListeners()
        {
            reproductionCostInput.onEndEdit.RemoveAllListeners();
            reproductionEfficiencyInput.onEndEdit.RemoveAllListeners();
            reproductionReserveInput.onEndEdit.RemoveAllListeners();
            reproductionCooldownInput.onEndEdit.RemoveAllListeners();
        }
    }
}