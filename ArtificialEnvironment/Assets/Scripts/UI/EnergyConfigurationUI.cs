using System.Globalization;
using Configuration;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class EnergyConfigurationUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField maxEnergyInput;
        [SerializeField] private TMP_InputField startingEnergyInput;
        [SerializeField] private TMP_InputField energyPerSecondInput;

        private EnergyConfiguration energyConfiguration;
        
        private void OnDestroy()
        {
            UnregisterListeners();
        }

        public void SetConfiguration(EnergyConfiguration energyConfiguration)
        {
            UnregisterListeners();
            this.energyConfiguration = energyConfiguration;
            
            maxEnergyInput.text = energyConfiguration.maxEnergy.ToString(CultureInfo.InvariantCulture);
            startingEnergyInput.text = energyConfiguration.startingEnergy.ToString(CultureInfo.InvariantCulture);
            energyPerSecondInput.text = energyConfiguration.energyPerSecond.ToString(CultureInfo.InvariantCulture);
            RegisterListeners();
        }

        private void RegisterListeners()
        {
            maxEnergyInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(maxEnergyInput, ref energyConfiguration.maxEnergy));
            startingEnergyInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(startingEnergyInput, ref energyConfiguration.startingEnergy));
            energyPerSecondInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(energyPerSecondInput, ref energyConfiguration.energyPerSecond));
        }

        private void UnregisterListeners()
        {
            maxEnergyInput.onEndEdit.RemoveAllListeners();
            startingEnergyInput.onEndEdit.RemoveAllListeners();
            energyPerSecondInput.onEndEdit.RemoveAllListeners();
        }
    }
}