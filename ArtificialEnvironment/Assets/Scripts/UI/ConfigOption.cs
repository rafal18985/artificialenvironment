using TMPro;
using UnityEngine;

namespace UI
{
    public class ConfigOption : MonoBehaviour
    {
        [SerializeField] private TMP_Text label;
        [SerializeField] private TMP_InputField inputField;
        

        public void RenameChildren()
        {
            if(label != null)
                label.name = $"{name}Label";
            if(inputField != null)
                inputField.name = $"{name}InputField";
        }
    }
}