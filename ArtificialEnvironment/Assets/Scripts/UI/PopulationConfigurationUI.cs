using System.Globalization;
using Configuration;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class PopulationConfigurationUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField maxPopulationInput;
        [SerializeField] private TMP_InputField desiredPopulationInput;

        private PopulationConfiguration populationConfiguration;

        private void OnDestroy()
        {
            UnregisterListeners();
        }

        public void SetConfiguration(PopulationConfiguration populationConfiguration)
        {
            UnregisterListeners();
            this.populationConfiguration = populationConfiguration;
            
            maxPopulationInput.text = populationConfiguration.maxPopulation.ToString(CultureInfo.InvariantCulture);
            desiredPopulationInput.text = populationConfiguration.desiredPopulation.ToString(CultureInfo.InvariantCulture);
            RegisterListeners();
        }

        private void RegisterListeners()
        {
            maxPopulationInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(maxPopulationInput, ref populationConfiguration.maxPopulation));
            desiredPopulationInput.onEndEdit.AddListener(_ => InputFieldUtils.TryUpdateValue(desiredPopulationInput, ref populationConfiguration.desiredPopulation));
        }

        private void UnregisterListeners()
        {
            maxPopulationInput.onEndEdit.RemoveAllListeners();
            desiredPopulationInput.onEndEdit.RemoveAllListeners();
        }
    }
}