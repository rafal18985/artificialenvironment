﻿using SO_Architecture.Collections;
using TMPro;
using UnityEngine;

public class PopulationInfoUI : MonoBehaviour
{
    [SerializeField] private TMP_Text plantsText;
    [SerializeField] private TMP_Text herbivoresText;
    [SerializeField] private TMP_Text carnivoresText;
    [SerializeField] private TMP_Text allText;
    
    [SerializeField] private ActorCollection plantsCollection;
    [SerializeField] private ActorCollection herbivoresCollection;
    [SerializeField] private ActorCollection carnivoresCollection;
    [SerializeField] private ActorCollection allCollection;

    private void Update()
    {
        plantsText.text = plantsCollection.Count.ToString();
        herbivoresText.text = herbivoresCollection.Count.ToString();
        carnivoresText.text = carnivoresCollection.Count.ToString();
        allText.text = allCollection.Count.ToString();
    }
}
