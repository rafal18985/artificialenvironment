﻿using System;

namespace Statistics
{
    [Serializable]
    public class FieldOfView
    {
        public float angle = 90;
        public float range = 2;

        public float MinAngle => -angle / 2;
        public float MaxAngle => angle / 2;
    }
}