using System;

namespace Statistics
{
    [Serializable]
    public class EyesightInfo
    {
        public float range = 30;
        public float[] angles = {-45.0f, 45.0f};
    }
}