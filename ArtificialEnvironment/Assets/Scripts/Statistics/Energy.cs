﻿using System;
using Configuration;
using UnityEngine;

namespace Statistics
{
    [Serializable]
    public class Energy
    {
        [SerializeField]
        private float value;

        public EnergyConfiguration energyConfiguration;

        public event Action OnAllEnergyLost;

        public float Value
        {
            get => value;
            private set
            {
                if (value.Equals(this.value))
                {
                    return;
                }

                this.value = Mathf.Clamp(value, 0, energyConfiguration.maxEnergy);

                if (this.value.Equals(0.0f))
                {
                    OnAllEnergyLost?.Invoke();
                }
            }
        }

        public void Spend(float amount)
        {
            amount = Math.Min(Value, amount);
            Value -= amount;
        }

        public void Gain(float amount)
        {
            Value += amount;
        }

        public void ModifyBy(float amount)
        {
            if (amount < 0)
            {
                Spend(-amount);
            }
            else
            {
                Gain(amount);
            }
        }

        public void TransferTo(Energy energy, float amount, float transferEfficiency = 1.0f)
        {
            amount = Math.Min(Value, amount);
            Spend(amount);
            energy.Gain(amount * transferEfficiency);
        }

        public void Reset()
        {
            value = energyConfiguration.startingEnergy;
        }
    }
}
