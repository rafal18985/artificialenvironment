﻿using Actors;
using Configuration;
using Generators.Base;
using Pools;
using SO_Architecture.Collections;
using Spawners;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LifeSupporters
{
    public abstract class LifeSupporter<T, TPool, TSpawner> : ScriptableObject where T : Actor
        where TPool : Pool<T>
        where TSpawner : Spawner<T, TPool>
    {
        public TSpawner spawner;

        public PopulationConfiguration populationConfiguration;
        public AnimationCurve spawnChanceCurve;

        public Vector3Generator randomPositionGenerator;
        public ActorCollection actorCollection;

        public void Update()
        {
            if(populationConfiguration.desiredPopulation < 1)
                return;
            
            if (Random.value < spawnChanceCurve.Evaluate(actorCollection.Count / (float) populationConfiguration.desiredPopulation))
            {
                Spawn();
            }
        }

        protected virtual void Spawn()
        {
            var position = randomPositionGenerator.Generate();
            if (spawner.CanSpawnAtPosition(position))
            {
                var actor = spawner.SpawnAtPosition(position);
            }
        }
    }
}
