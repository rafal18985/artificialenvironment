﻿using Actors;
using Pools;
using Spawners;
using UnityEngine;

namespace LifeSupporters
{
    [CreateAssetMenu(fileName = "PlantLifeSupporter", menuName = "LifeSupporters/Plant", order = 0)]
    public class PlantLifeSupporter : LifeSupporter<Plant, PlantPool, PlantSpawner>
    {
    }
}
