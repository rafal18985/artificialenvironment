﻿using Actors;
using Pools;
using Spawners;
using UnityEngine;

namespace LifeSupporters
{
    [CreateAssetMenu(fileName = "CreatureLifeSupporter", menuName = "LifeSupporters/Creature", order = 0)]
    public class CreatureLifeSupporter : LifeSupporter<Creature, CreaturePool, CreatureSpawner>
    {

    }
}
