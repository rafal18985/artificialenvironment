﻿using ScriptableObjectArchitecture;
using UnityEngine;
using Validators.Base;

namespace Validators
{
    [CreateAssetMenu(fileName = "InBoundsValidator", menuName = "Validators/InBounds", order = 0)]
    public class InBoundsValidator : Vector3Validator
    {
        public BoundsVariable bounds;

        public override bool IsValid(Vector3 obj)
        {
            return bounds.Value.Contains(obj);
        }
    }
}