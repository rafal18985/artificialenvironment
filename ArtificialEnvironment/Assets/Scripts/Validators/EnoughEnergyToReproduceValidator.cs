﻿using Actors;
using UnityEngine;
using Validators.Base;

namespace Validators
{
    [CreateAssetMenu(fileName = "EnoughEnergyToReproduceValidator", menuName = "Validators/Actors/Reproduce/EnoughEnergy", order = 0)]
    public class EnoughEnergyToReproduceValidator : ActorValidator
    {
        public override bool IsValid(Actor actor)
        {
            return actor.energy.Value >= actor.reproductionConfiguration.reproduceCost +
                   actor.reproductionConfiguration.energyReserve;
        }
    }
}
