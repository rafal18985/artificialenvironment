﻿using Actors;
using UnityEngine;
using Validators.Base;

namespace Validators
{
    [CreateAssetMenu(fileName = "ReproduceCooldownValidator", menuName = "Validators/Actors/Reproduce/Cooldown", order = 0)]
    public class ReproduceCooldownValidator : ActorValidator
    {
        public override bool IsValid(Actor actor)
        {
            return actor.timeSinceLastReproduce > actor.reproductionConfiguration.reproduceCooldown;
        }
    }
}
