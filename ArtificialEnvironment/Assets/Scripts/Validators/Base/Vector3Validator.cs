﻿using UnityEngine;

namespace Validators.Base
{
    public abstract class Vector3Validator : ScriptableObject, IValidator<Vector3>
    {
        public abstract bool IsValid(Vector3 obj);
    }
}