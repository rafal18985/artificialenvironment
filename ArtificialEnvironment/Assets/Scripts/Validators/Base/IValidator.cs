﻿namespace Validators.Base
{
    public interface IValidator<in T>
    {
        bool IsValid(T obj);
    }
}