﻿using Actors;
using Configuration;
using SO_Architecture.Collections;
using UnityEngine;

namespace Validators.Base
{
    [CreateAssetMenu(fileName = "CrowdLimiterValidator", menuName = "Validators/Actors/Reproduce/CrowdLimiter", order = 0)]
    public class CrowdLimiterValidator : ActorValidator
    {
        public ActorCollection actors;

        public PopulationConfiguration populationConfiguration;

        public override bool IsValid(Actor obj)
        {
            return actors.Count < populationConfiguration.maxPopulation;
        }
    }
}
