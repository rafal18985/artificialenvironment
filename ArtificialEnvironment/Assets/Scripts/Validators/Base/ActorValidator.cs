﻿using Actors;
using UnityEngine;

namespace Validators.Base
{
    public abstract class ActorValidator : ScriptableObject, IValidator<Actor>
    {
        public abstract bool IsValid(Actor obj);
    }
}
