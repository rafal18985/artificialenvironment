﻿using UnityEngine;
using Validators.Base;

namespace Validators
{
    [CreateAssetMenu(fileName = "SpaceAvailableValidator", menuName = "Validators/SpaceAvailable", order = 0)]
    public class SpaceAvailableValidator : Vector3Validator
    {
        public LayerMask mask;
        public Vector3 halfExtents;

        public override bool IsValid(Vector3 obj)
        {
            return !Physics.CheckBox(obj, halfExtents, Quaternion.identity, mask);
        }
    }
}