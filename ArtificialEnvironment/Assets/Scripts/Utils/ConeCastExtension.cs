﻿using System.Linq;
using UnityEngine;

namespace Utils
{
    public static class ConeCastExtension
    {
        public static bool CheckCone(Vector3 position, float range, Vector3 direction,
            float angle, int layerMask)
        {
            return Physics.OverlapSphere(position, range, layerMask)
                .Any(collider => IsInCone(position, direction, collider, angle));
        }

        public static Collider[] OverlapCone(Vector3 position, float range, Vector3 direction, float angle, int layerMask)
        {
            var colliders = Physics.OverlapSphere(position, range, layerMask)
                .Where(collider => IsInCone(position, direction, collider, angle)).ToArray();

            return colliders;
        }

        private static bool IsInCone(Vector3 position, Vector3 direction, Collider collider, float angle)
        {
            var dir = collider.transform.position - position;

            return Vector3.Angle(dir, direction) < angle;
        }
    }
}
