using System.Collections;
using UnityEngine;

namespace Utils
{
    public class CloseAfterSeconds : MonoBehaviour
    {
        [SerializeField] private float seconds;
        private void Start()
        {
            StartCoroutine(Close());
        }

        private IEnumerator Close()
        {
            yield return new WaitForSeconds(seconds);
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
        }
    }
}