using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SO_Architecture.Collections;
using UnityEngine;

namespace Utils
{
    public class PopulationSizeRecorder : MonoBehaviour
    {
        [SerializeField] private ActorCollection plantCollection;
        [SerializeField] private ActorCollection herbivoreCollection;
        [SerializeField] private ActorCollection carnivoreCollection;

        private readonly List<int[]> populationHistory = new List<int[]>();
        
        private string ConfigurationFileName => $"populationHistory_{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.csv";
        #if UNITY_EDITOR
        private string SavePath => $"{Application.dataPath}/{ConfigurationFileName}";
        #else
        private string SavePath => $"{Application.dataPath}/../{ConfigurationFileName}";
        #endif
        private void FixedUpdate()
        {
            populationHistory.Add(new []{plantCollection.Count, herbivoreCollection.Count, carnivoreCollection.Count});
        }

        private void OnApplicationQuit()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (var populationHistoryRow in populationHistory)
            {
                stringBuilder.AppendLine(String.Join(";", populationHistoryRow));
            }
            
            File.WriteAllText(SavePath, stringBuilder.ToString());
        }
    }
}