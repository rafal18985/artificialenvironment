using System;
using System.Globalization;
using TMPro;

namespace Utils
{
    public static class InputFieldUtils
    {
        public static void TryUpdateValue<T>(TMP_InputField inputField, ref T value) where T : IConvertible
        {   
            try
            {
                value = (T)Convert.ChangeType(inputField.text, typeof(T), CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                inputField.text = value.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}