using TMPro;
using UnityEngine.EventSystems;

namespace Utils
{
    public static class EventSystemUtils
    {
        public static bool IsUserTyping()
        {
            var currentCurrentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
            if(currentCurrentSelectedGameObject == null)
                return false;
            
            var inputField = currentCurrentSelectedGameObject.GetComponent<TMP_InputField>();
            if (inputField != null && !inputField.isFocused)
                return false;

            return true;
        }
    }
}