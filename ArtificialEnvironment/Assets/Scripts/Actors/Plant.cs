﻿using Configuration;
using Reproducers;
using UnityEngine;

namespace Actors
{
    public class Plant : Actor
    {
        public PlantReproducer reproducer;
        
        private void Start()
        {
            energy.OnAllEnergyLost += () => OnDied(ActorDiedEventArgs.DiedReason.Starvation);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            reproducer.TryToReproduce(this);
        }
    }
}
