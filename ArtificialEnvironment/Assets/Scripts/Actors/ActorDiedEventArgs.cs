﻿using System;

namespace Actors
{
    public class ActorDiedEventArgs : EventArgs
    {
        public DiedReason diedReason = DiedReason.Other;

        public enum DiedReason
        {
            Starvation,
            Other
        }
    }
}