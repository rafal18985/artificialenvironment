﻿using Configuration;
using Reproducers;
using UnityEngine;
using Utils;
using Validators;

namespace Actors
{
    public class Creature : Actor
    {
        [Header("Components")]
        public new Rigidbody rigidbody;

        public InBoundsValidator inBoundsValidator;
        public CreatureReproducer creatureReproducer;

        public CreatureAttributesConfiguration attributesConfiguration;


        public void Start()
        {
            energy.OnAllEnergyLost += () => OnDied(ActorDiedEventArgs.DiedReason.Starvation);
        }

        public override void OnPulledOut()
        {
            base.OnPulledOut();
            rigidbody.velocity = Vector3.zero;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (!energy.Value.Equals(0) && !inBoundsValidator.IsValid(transform.position))
                OnDied(ActorDiedEventArgs.DiedReason.Other);
        }

        public void Move(Vector3 dirToGo, Vector3 rotateDir)
        {
            rigidbody.velocity = attributesConfiguration.speed * dirToGo;
            transform.Rotate(rotateDir, Time.fixedDeltaTime * attributesConfiguration.turnSpeed);
        }

        public bool CanEat => ConeCastExtension.CheckCone(transform.position,
            attributesConfiguration.eatFieldOfView.range, transform.forward,
            attributesConfiguration.eatFieldOfView.angle, attributesConfiguration.eatMask);
        
        public bool Eat()
        {
            var food = FindFood();

            if(food == null)
                return false;

            food.energy.TransferTo(energy, attributesConfiguration.eatSpeed * Time.fixedDeltaTime);

            return true;
        }

        private Actor FindFood()
        {
            var colliders = ConeCastExtension.OverlapCone(transform.position,
                attributesConfiguration.eatFieldOfView.range, transform.forward,
                attributesConfiguration.eatFieldOfView.angle, attributesConfiguration.eatMask);
            if (colliders.Length == 0)
                return null;

            return colliders[0].gameObject.GetComponent<Actor>();
        }
    }
}