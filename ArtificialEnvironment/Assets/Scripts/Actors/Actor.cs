﻿using System;
using Configuration;
using Statistics;
using UnityEngine;

namespace Actors
{
    [SelectionBase]
    public abstract class Actor : MonoBehaviour
    {
        public ReproductionConfiguration reproductionConfiguration;

        public Energy energy;
        public float timeSinceLastReproduce = 0;

        public event EventHandler<ActorDiedEventArgs> Died;
        public event EventHandler PulledOut;


        public virtual void OnPulledOut()
        {
            energy.Reset();
            timeSinceLastReproduce = 0;
            
            PulledOut?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void FixedUpdate()
        {
            timeSinceLastReproduce += Time.fixedDeltaTime;
            energy.ModifyBy(energy.energyConfiguration.energyPerSecond * Time.fixedDeltaTime);
        }

        protected virtual void OnDied(ActorDiedEventArgs.DiedReason diedReason)
        {
            Died?.Invoke(this, new ActorDiedEventArgs(){diedReason = diedReason});
        }
    }
}
