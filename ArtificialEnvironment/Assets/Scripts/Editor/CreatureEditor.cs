﻿using System;
using Actors;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Creature)), CanEditMultipleObjects]
public class CreatureEditor : Editor
{
    void OnSceneGUI()
    {
        Creature creature = (Creature)target;

        Handles.color = new Color(1, 1, 1, 0.2f);

        var transform = creature.transform;
        var position = transform.position;
        Handles.DrawSolidArc(position, transform.up,
            Quaternion.AngleAxis(creature.attributesConfiguration.eatFieldOfView.MinAngle, transform.up) *
            transform.forward, creature.attributesConfiguration.eatFieldOfView.angle,
            creature.attributesConfiguration.eatFieldOfView.range);
        
        if (creature.attributesConfiguration.eyesightInfos != null)
        {
            Handles.color = Color.red;
            foreach (var eyesightInfo in creature.attributesConfiguration.eyesightInfos)
            {
                foreach (var angle in eyesightInfo.angles)
                {
                    Handles.DrawLine(position, position + transform.TransformDirection(CalculateDir(angle) * eyesightInfo.range));
                }
            }
        }
    }

    private Vector3 CalculateDir(float angle)
    {
        float angleInRad = angle * Mathf.Deg2Rad;
        return new Vector3(Mathf.Sin(angleInRad), 0, Mathf.Cos(angleInRad));
    }
}
