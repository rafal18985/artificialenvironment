using UI;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ConfigOption))]
public class ConfigOptionEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var configOption = (ConfigOption) target;
        if (GUILayout.Button("Rename children"))
        {
            configOption.RenameChildren();
        }
    }
}