﻿using LifeSupporters;
using Spawners;
using UnityEngine;

public class EnvironmentInitializer : MonoBehaviour
{
    public PlantSpawner plantSpawner;
    public Transform plantPoolParent;
    public CreatureSpawner herbivoreSpawner;
    public Transform herbivorePoolParent;
    public CreatureSpawner carnivoreSpawner;
    public Transform carnivorePoolParent;

    public PlantLifeSupporter plantLifeSupporter;
    public CreatureLifeSupporter herbivoreLifeSupporter;
    public CreatureLifeSupporter carnivoreLifeSupporter;
    
    void Start()
    {
        plantSpawner.pool.SetParent(plantPoolParent);
        herbivoreSpawner.pool.SetParent(herbivorePoolParent);
        carnivoreSpawner.pool.SetParent(carnivorePoolParent);
    }

    private void FixedUpdate()
    {
        plantLifeSupporter.Update();
        herbivoreLifeSupporter.Update();
        carnivoreLifeSupporter.Update();
    }
}