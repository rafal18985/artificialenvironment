﻿using System.Linq;
using Actors;
using Configuration;
using MLAgents;
using UnityEngine;

namespace ML
{
    [RequireComponent(typeof(Creature))]
    [RequireComponent(typeof(RayPerception3D))]
    public class CreatureAgent : Agent
    {
        [SerializeField] private RayPerception3D rayPerception;
        public Creature creature;
        public float multiActionSpeedModifier = 0.2f;

        public RewardConfiguration rewardConfiguration;

        public string[] detectableObjects = { "Plant", "Herbivore", "Carnivore" };

        private void Start()
        {
            creature.Died += OnDied;
        }

        public override void CollectObservations()
        {
            var position = transform.position;
            var bounds = creature.inBoundsValidator.bounds.Value;
            var localVelocity = transform.InverseTransformDirection(creature.rigidbody.velocity);

            if (rewardConfiguration.normalizeInput)
            {
                AddVectorObs(localVelocity.x / creature.attributesConfiguration.speed);
                AddVectorObs(localVelocity.z / creature.attributesConfiguration.speed);
                AddVectorObs((position.x - bounds.center.x) / bounds.size.x);
                AddVectorObs((position.z - bounds.center.z) / bounds.size.z);
                AddVectorObs(creature.energy.Value / creature.energy.energyConfiguration.maxEnergy);
            }
            else
            {
                AddVectorObs(localVelocity.x);
                AddVectorObs(localVelocity.z);
                AddVectorObs(position.x - bounds.center.x);
                AddVectorObs(position.z - bounds.center.z);
                AddVectorObs(creature.energy.Value);
            }

            foreach (var eyesightInfo in creature.attributesConfiguration.eyesightInfos)
            {
                AddVectorObs(rayPerception.Perceive(eyesightInfo.range,
                    eyesightInfo.angles.Select(f => f + 90).ToArray(),
                    detectableObjects, 0, 0));
            }
        }

        public override void AgentAction(float[] vectorAction, string textAction)
        {
            Vector3 dirToGo = transform.forward * Mathf.Clamp(vectorAction[3], -1f, 1f);
            Vector3 rotateDir = transform.up * Mathf.Clamp(vectorAction[4], -1f, 1f);

            bool wantToEat = false;
            bool wantToReproduce = false;
            
            if (vectorAction[1] > 0.5f)
            {
                wantToEat = true;
            }
            
            if(vectorAction[2] > 0.5f)
            {
                wantToReproduce = true;
            }

            if (wantToEat)
            {
                float currentEnergy = creature.energy.Value;
                if (creature.Eat())
                {
                    float energyIncome = creature.energy.Value - currentEnergy;
                    float maxEnergyIncome = (creature.attributesConfiguration.eatSpeed * Time.fixedDeltaTime);
                    float percentOfMaxEnergyIncome = maxEnergyIncome > 0 ? energyIncome / maxEnergyIncome : 0;
                    AddReward(rewardConfiguration.foodEatenReward * percentOfMaxEnergyIncome);

                    float overEat = 1 - percentOfMaxEnergyIncome;
                    AddReward(rewardConfiguration.overEatReward * overEat);
                }

                dirToGo *= multiActionSpeedModifier;
                rotateDir *= multiActionSpeedModifier;
            }

            if (wantToReproduce)
            {
                if(creature.creatureReproducer.TryToReproduce(creature))
                {
                    AddReward(rewardConfiguration.reproduceReward);
                }

                dirToGo *= multiActionSpeedModifier;
                rotateDir *= multiActionSpeedModifier;
            }
            
            creature.Move(dirToGo, rotateDir);
            
            AddReward(rewardConfiguration.existentialReward);
        }

        private void OnDied(object sender, ActorDiedEventArgs e)
        {
            SetReward(rewardConfiguration.dieReward);
            Done();
        }
    }
}