﻿using System.Collections.Generic;
using MLAgents;
using UnityEngine;

namespace ML
{
    public class DoNothingDecision : Decision
    {
        public override float[] Decide(List<float> vectorObs, List<Texture2D> visualObs, float reward, bool done, List<float> memory)
        {
            return new float[5];
        }

        public override List<float> MakeMemory(List<float> vectorObs, List<Texture2D> visualObs, float reward, bool done, List<float> memory)
        {
            return new List<float>();
        }
    }
}
