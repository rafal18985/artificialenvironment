using System;

namespace Configuration
{
    [Serializable]
    public class EnergyConfiguration
    {
        public float startingEnergy = 40;
        public float maxEnergy = 200;
        public float energyPerSecond = 0.5f;
    }
}