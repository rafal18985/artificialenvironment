using System;

namespace Configuration
{
    [Serializable]
    public class PopulationConfiguration
    {
        public int desiredPopulation = 10;
        public int maxPopulation = 400;
    }
}