using System;
using Statistics;
using UnityEngine;

namespace Configuration
{
    [Serializable]
    public class CreatureAttributesConfiguration
    {
        public float speed = 20;
        public float turnSpeed = 150;
        public float eatSpeed = 100;
        public FieldOfView eatFieldOfView = new FieldOfView()
        {
            angle = 45,
            range = 1.5f
        };
        public LayerMask eatMask;
        public EyesightInfo[] eyesightInfos;
    }
}