using System;

namespace Configuration
{
    [Serializable]
    public class ReproductionConfiguration
    {
        public float reproduceCost = 100;
        public float reproduceEfficiency = 0.4f;
        public float energyReserve = 20;
        public float reproduceCooldown = 0.5f;
    }
}