﻿using System;
using UnityEngine;

namespace Configuration
{
    [CreateAssetMenu(fileName = "TrainingConfiguration", menuName = "Configuration/Training", order = 0)]
    public class TrainingConfiguration : ScriptableObject
    {
        public ActorConfiguration plantConfiguration;
        public CreatureConfiguration herbivoreConfiguration;
        public CreatureConfiguration carnivoreConfiguration;

        public PopulationConfiguration allActorPopulationConfiguration;

        [TextArea]
        public string description;
        
        [Serializable]
        public class ActorConfiguration
        {
            public EnergyConfiguration energyConfiguration;
            public PopulationConfiguration populationConfiguration;
            public ReproductionConfiguration reproductionConfiguration;
        }
        
        [Serializable]
        public class CreatureConfiguration : ActorConfiguration
        {
            public CreatureAttributesConfiguration creatureAttributesConfiguration;
            public RewardConfiguration rewardConfiguration;
        }
    }
}
