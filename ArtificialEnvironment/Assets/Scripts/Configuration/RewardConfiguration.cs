using System;

namespace Configuration
{
    [Serializable]
    public class RewardConfiguration
    {
        public float dieReward = -1;
        public float foodEatenReward = 0.05f;
        public float overEatReward = -0.25f;
        public float reproduceReward = 1;
        public float existentialReward = -0.0001f;
        public bool normalizeInput = true;
    }
}