using Actors;
using Pools;
using UnityEngine;

namespace Spawners
{
    public interface ISpawner<out T, out TPool> where T : Actor where TPool : IPool<T>
    {
        T SpawnAtPosition(Vector3 position);
        bool CanSpawnAtPosition(Vector3 position);
    }
}