﻿using Actors;
using Pools;
using UnityEngine;

namespace Spawners
{
    [CreateAssetMenu(fileName = "CreatureSpawner", menuName = "Spawners/Creature", order = 0)]
    public class CreatureSpawner : Spawner<Creature, CreaturePool>
    {
    }
}
