﻿using System.Collections.Generic;
using System.Linq;
using Actors;
using Pools;
using UnityEngine;
using Validators.Base;

namespace Spawners
{
    public abstract class Spawner<T, TPool> : ScriptableObject, ISpawner<T, TPool> where T : Actor where TPool : IPool<T>
    {
        public TPool pool;

        public List<Vector3Validator> positionValidators;

        public T SpawnAtPosition(Vector3 position)
        {
            if (!CanSpawnAtPosition(position))
            {
                return null;
            }

            var actor = pool.Get();
            actor.transform.position = position;
            actor.gameObject.SetActive(true);

            return actor;
        }

        public bool CanSpawnAtPosition(Vector3 position)
        {
            return positionValidators.All(validator => validator.IsValid(position));
        }
    }
}
