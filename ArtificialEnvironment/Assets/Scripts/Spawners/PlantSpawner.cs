﻿using Actors;
using Pools;
using UnityEngine;

namespace Spawners
{
    [CreateAssetMenu(fileName = "PlantSpawner", menuName = "Spawners/Plant", order = 0)]
    public class PlantSpawner : Spawner<Plant, PlantPool>
    {
    }
}
