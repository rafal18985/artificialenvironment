using Generators.Base;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Generators
{
    [CreateAssetMenu(fileName = "InRadiusInBoundsGenerator", menuName = "Generators/InRadiusInBounds", order = 0)]
    public class InRadiusInBoundsGenerator : Vector3Generator
    {
        public BoundsVariable bounds;
        public float minRadius = 2;
        public float maxRadius = 5;
        public override Vector3 Generate()
        {
            return Generate(Vector3.zero);
        }

        public Vector3 Generate(Vector3 position, int maxTries = 5)
        {
            var output = Vector3.zero;
            if (!bounds.Value.Contains(position))
                return output;

            int i = 0;
            do
            {
                var direction = Random.insideUnitCircle.normalized;
                var offset = direction * Random.Range(minRadius, maxRadius);
                
                output.x = position.x + offset.x;
                output.z = position.z + offset.y;
                i++;
            } while (!bounds.Value.Contains(output) && i < maxTries);

            return output;
        }
    }
}