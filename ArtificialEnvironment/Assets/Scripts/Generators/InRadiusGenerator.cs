﻿using Generators.Base;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Generators
{
    [CreateAssetMenu(fileName = "InRadiusGenerator", menuName = "Generators/InRadius", order = 0)]
    public class InRadiusGenerator : Vector3Generator
    {
        public FloatReference minRadius;
        public FloatReference maxRadius;
        public override Vector3 Generate()
        {
            var direction = Random.insideUnitCircle.normalized;
            var offset = direction * Random.Range(minRadius.Value, maxRadius.Value);
            return new Vector3(offset.x, 0, offset.y);
        }
    }
}
