﻿namespace Generators.Base
{
    public interface IGenerator<out T>
    {
        T Generate();
    }
}
