﻿using UnityEngine;

namespace Generators.Base
{
    public abstract class Vector3Generator : ScriptableObject, IGenerator<Vector3> 
    {
        public abstract Vector3 Generate();
    }
}
