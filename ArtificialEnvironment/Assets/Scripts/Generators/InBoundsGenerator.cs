﻿using Generators.Base;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Generators
{
    [CreateAssetMenu(fileName = "InBoundsGenerator", menuName = "Generators/InBounds", order = 0)]
    public class InBoundsGenerator : Vector3Generator
    {
        public BoundsVariable bounds;
        public bool overrideY;
        public float constY;
        public override Vector3 Generate()
        {
            var position = new Vector3(Random.Range(bounds.Value.min.x, bounds.Value.max.x),
                Random.Range(bounds.Value.min.y, bounds.Value.max.y),
                Random.Range(bounds.Value.min.z, bounds.Value.max.z));
            if (overrideY)
            {
                position.y = constY;
            }

            return position;
        }
    }
}
