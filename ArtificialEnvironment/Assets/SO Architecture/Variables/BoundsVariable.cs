using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[CreateAssetMenu(
	    fileName = "BoundsVariable.asset",
	    menuName = SOArchitecture_Utility.VARIABLE_SUBMENU + "Advanced/Bounds",
	    order = 120)]
	public class BoundsVariable : BaseVariable<Bounds>
	{
	}
}