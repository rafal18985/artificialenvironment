﻿using Actors;
using SO_Architecture.Collections;
using UnityEngine;

namespace SO_Architecture.CollectionAdders
{
    public class ActorCollectionAdder : MonoBehaviour
    {
        [SerializeField]
        private ActorCollection targetCollection;

        [SerializeField]
        private Actor actor;

        private void OnEnable()
        {
            targetCollection.Add(actor);
        }
        private void OnDisable()
        {
            targetCollection.Remove(actor);
        }
    }
}
