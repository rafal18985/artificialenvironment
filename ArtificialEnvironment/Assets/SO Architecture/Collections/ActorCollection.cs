using Actors;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace SO_Architecture.Collections
{
	[CreateAssetMenu(
	    fileName = "ActorCollection.asset",
	    menuName = SOArchitecture_Utility.COLLECTION_SUBMENU + "Actors/Actor",
	    order = 120)]
	public class ActorCollection : Collection<Actor>
	{
	}
}