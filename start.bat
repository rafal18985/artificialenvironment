@echo off
echo mlagents-learn config/trainer_config.yaml --run-id=PoliceVsThief1 --train
echo mlagents-learn config/trainer_config.yaml --env="envs/LearningEnvironment/Unity Environment" --run-id=PoliceVsThief1 --num-envs=8 --train
echo mlagents-learn config/trainer_config.yaml --curriculum=config/curricula/PoliceVsThief/ --env="envs/LearningEnvironment/Unity Environment" --run-id=PoliceVsThief1 --num-envs=8 --train

call conda activate ml-agents & powershell
