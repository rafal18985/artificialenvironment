Write-Host mlagents-learn config/trainer_config.yaml --run-id=PoliceVsThief1 --train
Write-Host mlagents-learn config/trainer_config.yaml --env="envs/LearningEnvironment/Unity Environment" --run-id=PoliceVsThief1 --num-envs=8 --train
Write-Host mlagents-learn config/trainer_config.yaml --curriculum=config/curricula/PoliceVsThief/ --env="envs/LearningEnvironment/Unity Environment" --run-id=PoliceVsThief1 --num-envs=8 --train

conda activate ml-agents